This is a real-time version of the Battleship Game. Details Below.

---

# Features

* **Login page**

![Alt text](https://i.imgur.com/uAsaTFb.png "Login")

You can use any of the following existing users:

  * username: pierre, password: 123
  * username: angie, password: 123
  * username: yess, password: 123
  * username: akira, password: 123
  * username: carlos, password: 123

---

* **Home page**

![Alt text](https://i.imgur.com/SrxIpAT.png "Home")

  * Create new Game: create game and invite others to join by clicking 'New Game' on the nav header
  * Games pool: pick a game created by others and play
  * My current games: resume ongoing games

---

* **Finished games page**

![Alt text](https://i.imgur.com/po501jT.png "Finished Games")

  * Check the details of your finished games

---

* **Game page**

![Alt text](https://i.imgur.com/EV1xOqY.png "Game")


  * Battleship board
  * Live interactions at the right of the board
  * Surrender option

---

* **Responsive Layout**

![Alt text](https://i.imgur.com/sAWVPFL.png "Responsive")


  * Responsive layout and pages

---

# Get Started


**1. Clone the project**

    git clone https://pierrealni@bitbucket.org/pierrealni/battleshipclient.git


**2. Install dependencies**


    npm install  

**3. Run the server**

    See details on https://bitbucket.org/pierrealni/battleship_server/src/master/

**4. Run this client side app**


    npm start -s

    This will run the automated build process, start up a webserver, and open the application 
    in the default browser. This command will continue watching all the source files. Every 
    time you hit save the code is rebuilt. Note: The -s flag is optional. It enables silent 
    mode which suppresses unnecessary messages during the build.


---

# Test

**Run the test command**

    ng test

Tests output:

![Alt text](https://i.imgur.com/yee1Inc.png "Test")

Automatically generated index.html on coverage folder:

![Alt text](https://i.imgur.com/JLyjduF.png "Home")

---

# Technologies


This client side of the Battleship App is the result of the sum of the following technologies:


| **Name**  | **Description**|
|---------- |----------------|
| Angular     | Platform for building mobile and desktop web applications |
| Angular Apollo | Library meant to be the best way to use GraphQL with Angular, to build client applications |
| Angular Material | UI component infrastructure and Material Design components for mobile and desktop Angular web applications |
| SASS | Compiled CSS styles with variables, functions, and more |
