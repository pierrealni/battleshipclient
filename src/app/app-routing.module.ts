import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from '@shared/services/auth-guard-service';
import { BattleshipGameComponent } from '@pages/battleship-game/battleship-game.component';
import { FinishedGamesComponent } from '@pages/finished-games/finished-games.component';
import { GamesPoolComponent } from '@pages/games-pool/games-pool.component';
import { LayoutComponent } from '@shared/components/layout/layout.component';
import { LoginComponent } from '@pages/login/login.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: GamesPoolComponent
      },
      {
        path: 'finished',
        component: FinishedGamesComponent
      },
      {
        path: 'game/:id',
        component: BattleshipGameComponent
      },
      {
        path: 'game',
        component: BattleshipGameComponent
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '**',
    redirectTo: '/home',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
