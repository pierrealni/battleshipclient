import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { getMainDefinition } from '@apollo/client/utilities';
import { split, ApolloClientOptions, InMemoryCache } from '@apollo/client/core';
import { WebSocketLink } from '@apollo/client/link/ws';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BattleshipGameComponent } from '@pages/battleship-game/battleship-game.component';
import { BoardComponent } from '@pages/battleship-game/board/board.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FinishedGamesComponent } from '@pages/finished-games/finished-games.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GamesPoolComponent } from '@pages/games-pool/games-pool.component';
import { HeaderComponent } from '@shared/components/layout/navigation/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { LayoutComponent } from '@shared/components/layout/layout.component';
import { LoginComponent } from '@pages/login/login.component';
import { MaterialModule } from '@shared/material.module';
import { SidenavListComponent } from '@shared/components/layout/navigation/sidenav-list/sidenav-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    SidenavListComponent,
    GamesPoolComponent,
    FinishedGamesComponent,
    BattleshipGameComponent,
    BoardComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory(httpLink: HttpLink): ApolloClientOptions<any> {
        // Create an http link:
        const http = httpLink.create({
          uri: 'http://localhost:3030/graphql',
        });

        // Create a WebSocket link:
        const ws = new WebSocketLink({
          uri: `ws://localhost:3030/graphql`,
          options: {
            reconnect: true,
          },
        });

        // using the ability to split links, you can send data to each link
        // depending on what kind of operation is being sent
        const link = split(
          // split based on operation type
          ({query}) => {
            const { kind, operation } = getMainDefinition(query) as any;
            return (
              kind === 'OperationDefinition' && operation === 'subscription'
            );
          },
          ws,
          http,
        );

        return {
          link,
          cache: new InMemoryCache()
        };
      },
      deps: [HttpLink]
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
