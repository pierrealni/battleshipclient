import gql from 'graphql-tag';

export const GAME_QUERIES = {
  fetchGameQuery: gql`
    query fetchGame($id: ID!) {
      fetchGame(id: $id) {
        id
        createdAt
        player1 {
          id
          name
        },
        player2 {
          id
          name
        },
        turnsPlayed
        currentTurn {
          id
          name
        }
        status
        settings
      }
    }
  `,
  fetchGamesQuery: gql`
    query fetchGames {
      fetchGames {
        id
        createdAt
        player1 {
          id
          name
        },
        player2 {
          id
          name
        },
        turnsPlayed
        currentTurn {
          id
          name
        }
        status
      }
    }
  `,
  fetchMyFinishedGamesQuery: gql`
    query fetchMyFinishedGames($id: ID!) {
      fetchMyFinishedGames(id: $id) {
        id
        createdAt
        player1 {
          id
          name
        },
        player2 {
          id
          name
        },
        turnsPlayed
        currentTurn {
          id
          name
        }
        status
      }
    }
  `,
  fetchPlayerLoginQuery: gql`
    query fetchPlayerLogin($name: String!, $password: String!) {
      fetchPlayerLogin(name: $name, password: $password) {
        id
        name
      }
    }
  `,
  fetchPlayerQuery: gql`
    query fetchPlayer($id: ID!) {
      fetchPlayer(id: $id) {
        id
        name
      }
    }
  `
};

export const GAME_MUTATIONS = {
  startGame: gql`
    mutation startGame($game: NewGameInput!) {
      startGame(game: $game) {
        response
        matrix
      }
    }`,
  joinGame: gql`
    mutation joinGame($game: NewGameInput!) {
      joinGame(game: $game)  {
        response
        matrix
        actionAdded
      }
    }`,
  surrenderGame: gql`
    mutation surrenderGame($surrender: SurrenderInput!) {
        surrenderGame(surrender: $surrender)
    }`,
  makeMove: gql`
    mutation makeMove($move: MoveInput!) {
      makeMove(move: $move)
    }`,
  createPlayer: gql`
    mutation createPlayer($player: PlayerInput!) {
      createPlayer(player: $player){
        id
        name
      }
    }`,
};

export const GAME_SUBS = {
  gameAddedSubs: gql`
    subscription gameAdded {
      gameAdded {
        id
        createdAt
        player1 {
          id
          name
        },
        player2 {
          id
          name
        },
        turnsPlayed
        currentTurn {
          id
          name
        }
        status
      }
    }`,
  actionAdded: gql`
    subscription actionAdded($gameId: ID!) {
      actionAdded(gameId: $gameId)
    }`
};
