import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Apollo } from 'apollo-angular';
import { Subscription} from 'rxjs';

import { BattleshipGameComponent } from './battleship-game.component';
import { apolloServiceStub, joinGameResponse, mockGame, mockMatrix, mockMove, mockUser } from '@shared/mock/operations.mock';
import { GAME_MUTATIONS, GAME_QUERIES } from '@graphql/operations';
import GAME_CONSTANTS from '@shared/game.constants';

describe('BattleshipGameComponent', () => {
  let component: BattleshipGameComponent;
  let fixture: ComponentFixture<BattleshipGameComponent>;
  const routerServiceStub = {
    navigate: jasmine.createSpy('navigate')
  };
  const routeServiceStub = {
    snapshot: {
      paramMap: {
        get: jasmine.createSpy('snapshot').and.returnValue('test')
      }
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BattleshipGameComponent ],
      providers: [
        { provide: Apollo, useValue: apolloServiceStub },
        { provide: Router, useValue: routerServiceStub },
        { provide: ActivatedRoute, useValue: routeServiceStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BattleshipGameComponent);
    component = fixture.componentInstance;
    component.actionsSubscription = new Subscription();
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('Game Operations', () => {
    beforeEach(() => {
      component.currentUser = mockUser;
      component.gameId = mockGame.id;
      component.matrix = mockMatrix;
      component.playerSide = 1;
    });

    it('should join a game', async () => {
      await component.joinGame();
      fixture.detectChanges();

      expect(component.apollo.mutate).toHaveBeenCalledWith({
        mutation: GAME_MUTATIONS.joinGame,
        variables: { game : { id: component.gameId, playerId: component.currentUser.id } }
      });
      expect(component.opponentName).toBe(joinGameResponse.actionAdded.opponentName);
    });

    it('should get a game', async () => {
      await component.getGame();
      fixture.detectChanges();

      expect(component.apollo.query).toHaveBeenCalledWith({
        query: GAME_QUERIES.fetchGameQuery,
        variables: { id: component.gameId }
      });
    });

    it('should make a move', async () => {
      await component.makeMove(mockMove);
      fixture.detectChanges();

      expect(component.apollo.mutate).toHaveBeenCalledWith({
        mutation: GAME_MUTATIONS.makeMove,
        variables: { move: { gameId: component.gameId, ...mockMove, playerIndex: component.playerSide } }
      });
      expect(component.matrix[mockMove.x][mockMove.y]).toBe(mockMove.newStatus);
    });

    it('should surrender', async () => {
      spyOn(window, 'confirm').and.returnValue(true);
      await component.surrenderGame();
      fixture.detectChanges();

      expect(component.apollo.mutate).toHaveBeenCalledWith({
        mutation: GAME_MUTATIONS.surrenderGame,
        variables: { surrender: { gameId: component.gameId, playerId: component.currentUser.id } }
      });
      expect(routerServiceStub.navigate).toHaveBeenCalledWith(['/finished']);
    });

    it('should render correct game status', async () => {
      const currentTurnId = '-1';
      let response;

      component.currentTurnId = currentTurnId;
      response = component.renderStatus();

      expect(response).toBe(GAME_CONSTANTS.game_status.waiting_opponent);
    });
  });
});
