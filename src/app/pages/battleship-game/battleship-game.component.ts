import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';

import { Apollo } from 'apollo-angular';
import { ApolloQueryResult, FetchResult } from '@apollo/client/core';
import { Subscription } from 'rxjs';

import { AuthService } from '@shared/services/auth.service';
import {
  ActionAdded,
  ActionAddedSubscriptionModel,
  FetchGameQueryModel,
  JoinGameMutationModel,
  MakeMoveMutationModel,
  StartGameMutationModel,
  SurrenderGameMutationModel
} from '@shared/models/operations.model';
import { CommonService } from '@shared/services/common.service';
import GAME_CONSTANTS from '@shared/game.constants';
import { GAME_MUTATIONS, GAME_QUERIES, GAME_SUBS } from '@graphql/operations';
import { UserModel } from '@shared/models/user.model';
import { MoveModel } from '@shared/models/move.model';

@Component({
  selector: 'app-battleship-game',
  templateUrl: './battleship-game.component.html',
  styleUrls: ['./battleship-game.component.scss']
})
export class BattleshipGameComponent implements OnInit, OnDestroy {
  constants = GAME_CONSTANTS;
  gameId = '';
  currentUser: UserModel;
  playerSide: number;
  currentTurnId: string;
  matrix: Array<Array<number>>;
  opponentName = '';
  gameReady = false;
  actionsStack: Array<ActionAdded> = [];
  actionInProgress = false;
  actionsSubscription: Subscription;

  constructor(public apollo: Apollo,
              public auth: AuthService,
              public route: ActivatedRoute,
              public commonService: CommonService,
              public router: Router) {
    this.currentUser = this.auth.getCurrentUser();
  }

  async ngOnInit(): Promise<void> {
    const { id: USERID } = this.currentUser;
    this.gameId = this.route.snapshot.paramMap.get('id') as string;
    let game;

    if (!this.gameId) {
      await this.createGame();
    } else {
      if (this.gameId === 'test') return;

      game = await this.getGame();
      if (!game) {
        alert('Game id not found');
        this.router.navigate(['/home']);

        return;
      } else {
        switch (true) {
          case game.player1.id === USERID: // if current user is player 1
            this.matrix = game.settings.matrix1;
            this.opponentName = (game.player2.id !== '-1' ? game.player2.name : '' );
            this.playerSide = 1;
            this.currentTurnId = game.currentTurn.id;
            this.gameReady = true;
            break;
          case game.player2.id === USERID: // if current user is player 2
            this.matrix = game.settings.matrix2;
            this.opponentName = game.player1.name;
            this.playerSide = 2;
            this.currentTurnId = game.currentTurn.id;
            this.gameReady = true;
            break;
          case game.player2.id === '-1': // if current user is entering a pool game as opponent
            await this.joinGame();
            break;
          default:
          // user is viewer of an existing game where he/she is not player1 or player2
        }
      }
    }
    this.onPlayerAction();
  }

  ngOnDestroy(): void {
    this.actionsSubscription.unsubscribe();
  }

  renderStatus = () => {
    let status = '';

    switch (this.currentTurnId) {
      case '-1':
        status = this.constants.game_status.waiting_opponent;
        break;
      case this.currentUser.id:
        status = this.constants.game_status.your_turn;
        break;
      default:
        status = this.constants.game_status.opponent_turn;
    }

    return status;
  }

  getGame = async () => {
    const response: ApolloQueryResult<FetchGameQueryModel> = await this.apollo.query<FetchGameQueryModel>({
      query: GAME_QUERIES.fetchGameQuery,
      variables: { id: this.gameId }
    }).toPromise();
    const { fetchGame } = response.data;

    this.commonService.log('fetchGame', fetchGame);

    return fetchGame;
  }

  createGame = async () => {
    const gameId = Date.now().toString();
    const newGame = { id: gameId, playerId: this.currentUser.id };
    const response: FetchResult<StartGameMutationModel> = await this.apollo.mutate<StartGameMutationModel>({
      mutation: GAME_MUTATIONS.startGame,
      variables: { game: newGame }
    }).toPromise();
    const { startGame } = response.data as StartGameMutationModel;

    this.gameId = gameId;
    this.playerSide = 1;
    this.currentTurnId = '-1';
    this.matrix = startGame.matrix;
    this.gameReady = true;
    this.commonService.log('startGame', startGame);
  }

  joinGame = async () => {
    const { id: USERID } = this.currentUser;
    const game = { id: this.gameId, playerId: USERID };
    const response: FetchResult<JoinGameMutationModel> = await this.apollo.mutate<JoinGameMutationModel>({
      mutation: GAME_MUTATIONS.joinGame,
      variables: { game }
    }).toPromise();
    const { joinGame } = response.data as JoinGameMutationModel;

    this.matrix = joinGame.matrix;
    this.opponentName = joinGame.actionAdded.opponentName as string;
    this.playerSide = 2;
    this.currentTurnId = USERID;
    this.gameReady = true;
    this.addActionToStack(joinGame.actionAdded);
    this.commonService.log('joinGame', joinGame);
  }

  addActionToStack = (action: ActionAdded) => {
    const localActionStacks = [...this.actionsStack];

    localActionStacks.push(action);
    if (localActionStacks.length > 9) {
      localActionStacks.shift();
    }
    this.actionsStack = localActionStacks;

    switch (true) {
      case (action.gameIsSurrendered && action.actorId !== this.currentUser.id):
        alert('Game is finished, ' + action.actionDesc);
        setTimeout(() => {
          this.commonService.log('Go to finished games');
          this.router.navigate(['/finished']);
        }, 1000);
        break;
      case(action.gameHasWinner) :
        alert('Game is finished, ' + action.actionDesc);
        setTimeout(() => {
          this.commonService.log('Go to finished games');
          this.router.navigate(['/finished']);
        }, 1000);
        break;
      default:
        if (action.nextTurnId) {
          this.currentTurnId = action.nextTurnId;
        }
        if (action.opponentName) {
          this.opponentName = action.opponentName;
        }
    }
  }

  surrenderGame = async () => {
    const modal = confirm(this.constants.messages.confirm_surrender);
    const surrenderGamePayload = { gameId: this.gameId, playerId: this.currentUser.id };
    let surrenderGameResponseWrapper: FetchResult<SurrenderGameMutationModel>;
    let surrenderGame: string;

    if (!modal) {
      return;
    }

    this.actionInProgress = true;
    surrenderGameResponseWrapper = await this.apollo.mutate<SurrenderGameMutationModel>({
      mutation: GAME_MUTATIONS.surrenderGame,
      variables: { surrender: surrenderGamePayload }
    }).toPromise();
    surrenderGame = (surrenderGameResponseWrapper.data as SurrenderGameMutationModel).surrenderGame;

    this.actionInProgress = false;
    this.commonService.log('surrenderGame', surrenderGame);
    this.commonService.log('Go to finished games');
    this.router.navigate(['/finished']);
  }

  makeMove = async (move: MoveModel) => {
    const { x, y, newStatus } = move;
    const matrix = this.matrix.map((arr: Array<number>) => {
      return arr.slice();
    });
    const makeMovePayload = { gameId: this.gameId, x, y, newStatus, playerIndex: this.playerSide };
    let makeMoveResponse: FetchResult<MakeMoveMutationModel>;
    let makeMove: string;

    // local update
    matrix[x][y] = newStatus;
    this.matrix = matrix;
    // server update
    this.actionInProgress = true;
    makeMoveResponse = await this.apollo.mutate<MakeMoveMutationModel>({
      mutation: GAME_MUTATIONS.makeMove,
      variables: { move: makeMovePayload }
    }).toPromise();
    makeMove = (makeMoveResponse.data as MakeMoveMutationModel).makeMove;

    this.actionInProgress = false;
    this.commonService.log('makeMove', makeMove);
  }

  onPlayerAction = () => {
    let actionAdded: ActionAdded;

    this.actionsSubscription = this.apollo.subscribe<ActionAddedSubscriptionModel>({
      query: GAME_SUBS.actionAdded,
      variables: { gameId: this.gameId },
    }).subscribe({
      next: (next: FetchResult<ActionAddedSubscriptionModel>) => {
        actionAdded = (next.data as ActionAddedSubscriptionModel).actionAdded;
        this.commonService.log('actionAdded', actionAdded);
        if (actionAdded) {
          this.addActionToStack(actionAdded);
        }
      },
      error: (err) => {
        this.commonService.log('err', err);
      }
    });
  }
}
