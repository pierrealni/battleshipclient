import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { BoardComponent } from './board.component';
import { mockMatrix } from '@shared/mock/operations.mock';
import GAME_CONSTANTS from '@shared/game.constants';

describe('BoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    component.matrix = mockMatrix;
    component.clickable = true;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have the correct css class', () => {
    const x = 0;
    const y = 0;
    const className = component.getClassName(x, y);

    expect(component.matrix[x][y]).toBe(GAME_CONSTANTS.cell_status.COVERED_NO_SHIP);
    expect(className.indexOf(GAME_CONSTANTS.cell_class_names.covered) !== -1).toBe(true);
  });

  it('should emit cell click', () => {
    const buttons = fixture.nativeElement.querySelectorAll('button');
    const firstButton = buttons[0];
    const spy = spyOn(component.onClickCell, 'emit');

    firstButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(spy).toHaveBeenCalledWith({ x: 0, y: 0, newStatus: GAME_CONSTANTS.cell_status.MISS });
  });
});
