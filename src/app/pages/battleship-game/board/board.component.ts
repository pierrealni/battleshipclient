import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import GAME_CONSTANTS from '@shared/game.constants';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  constants = GAME_CONSTANTS;

  constructor() { }
  @Input() matrix: Array<Array<number>>;
  @Input() clickable = false;
  @Output() onClickCell = new EventEmitter();

  ngOnInit(): void {
  }

  getClassName(i: number, j: number): string {
    const value = this.matrix[i][j];
    const cellStatus = this.constants.cell_status;
    const cellClassNames = this.constants.cell_class_names;
    let className = '';

    if (value >= cellStatus.COVERED_NO_SHIP) {
      className += ` ${cellClassNames.covered}`;
    }
    if (value === cellStatus.MISS) {
      className += ` ${cellClassNames.miss}`;
    }
    if (value === cellStatus.HITTED) {
      className += ` ${cellClassNames.hitted}`;
    }
    if (!this.clickable) {
      className += ` ${cellClassNames.not_allowed}`;
    }

    return className;
  }

  cellClick(x: number, y: number, value: number): void {
    const cellStatus = this.constants.cell_status;

    if (value !== cellStatus.MISS && value !== cellStatus.HITTED && this.clickable) {
      if (value === cellStatus.COVERED_NO_SHIP) {
        this.onClickCell.emit({x, y, newStatus: cellStatus.MISS});
      } else if (value > cellStatus.COVERED_NO_SHIP) {
        this.onClickCell.emit({x, y, newStatus: cellStatus.HITTED});
      }
    }
  }
}
