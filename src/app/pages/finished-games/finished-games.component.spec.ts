import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { Subscription } from 'rxjs';
import { Apollo } from 'apollo-angular';

import { FinishedGamesComponent } from './finished-games.component';
import { apolloServiceStub, mockUser } from '@shared/mock/operations.mock';
import { GAME_QUERIES } from '@graphql/operations';

describe('FinishedGamesComponent', () => {
  let component: FinishedGamesComponent;
  let fixture: ComponentFixture<FinishedGamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinishedGamesComponent ],
      providers: [
        { provide: Apollo, useValue: apolloServiceStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedGamesComponent);
    component = fixture.componentInstance;
    component.myFinishedGamesSubscription = new Subscription();
    component.currentUser = mockUser;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should called fetchGames and fill data table', () => {
    expect(component.apollo.watchQuery).toHaveBeenCalledWith({
      query: GAME_QUERIES.fetchMyFinishedGamesQuery,
      variables: { id: component.currentUser.id },
      pollInterval: 3000
    });
    expect(component.tableDataSource.length).toBeGreaterThan(0);
  });
});
