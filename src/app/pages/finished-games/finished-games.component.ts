import { Component, OnDestroy, OnInit } from '@angular/core';

import { Apollo } from 'apollo-angular';
import { concatMap, tap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';

import { AuthService } from '@shared/services/auth.service';
import { CommonService } from '@shared/services/common.service';
import { FetchGame, FetchMyFinishedGamesQueryModel } from '@shared/models/operations.model';
import { GAME_QUERIES } from '@graphql/operations';
import { UserModel } from '@shared/models/user.model';

@Component({
  selector: 'app-finished-games',
  templateUrl: './finished-games.component.html',
  styleUrls: ['./finished-games.component.scss']
})
export class FinishedGamesComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['index', 'createdAt', 'player1.name', 'player2.name', 'turnsPlayed', 'status'];
  tableDataSource: Array<FetchGame>;
  currentUser: UserModel;
  loading = true;
  myFinishedGamesSubscription: Subscription;

  constructor(public apollo: Apollo,
              public auth: AuthService,
              public commonService: CommonService) {
    this.currentUser = this.auth.getCurrentUser();
  }

  ngOnInit(): void {
    const { id: USERID } = this.currentUser;
    let fetchMyFinishedGames: Array<FetchGame>;

    this.myFinishedGamesSubscription = this.apollo
      .watchQuery<FetchMyFinishedGamesQueryModel>({
        query: GAME_QUERIES.fetchMyFinishedGamesQuery,
        variables: {
          id: USERID
        },
        pollInterval: 3000
      })
      .valueChanges
      .pipe(
        concatMap((value, index) => index === 0 ?
          of(value).pipe(
            tap(() =>  {
              this.loading = false;
            })
          ) : of(value)
        )
      )
      .subscribe((result) => {
        fetchMyFinishedGames = result.data.fetchMyFinishedGames;
        this.tableDataSource = fetchMyFinishedGames;
      });
  }

  ngOnDestroy(): void {
    this.myFinishedGamesSubscription.unsubscribe();
  }

  formatDate = (date: string) => this.commonService.formatDate(date);
}
