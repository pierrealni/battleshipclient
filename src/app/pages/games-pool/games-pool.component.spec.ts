import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { Apollo } from 'apollo-angular';

import { GamesPoolComponent } from './games-pool.component';
import { apolloServiceStub, mockUser } from '@shared/mock/operations.mock';
import { GAME_QUERIES } from '@graphql/operations';

describe('GamesPoolComponent', () => {
  let component: GamesPoolComponent;
  let fixture: ComponentFixture<GamesPoolComponent>;
  const routerServiceStub = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamesPoolComponent ],
      providers: [
        { provide: Apollo, useValue: apolloServiceStub },
        { provide: Router, useValue: routerServiceStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesPoolComponent);
    component = fixture.componentInstance;
    component.gameSubscription = new Subscription();
    component.currentUser = mockUser;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should call fetchGames and fill data tables', () => {
    expect(component.apollo.watchQuery).toHaveBeenCalledWith({
      query: GAME_QUERIES.fetchGamesQuery,
      variables: { id: component.currentUser.id },
      pollInterval: 3000
    });
    expect(component.currentGames.length).toBeGreaterThan(0);
    expect(component.poolGames.length).toBeGreaterThan(0);
  });

  it('should go to play board', () => {
    const gameId = 'test';

    component.play(gameId);

    expect(routerServiceStub.navigate).toHaveBeenCalledWith(['/game/' + gameId]);
  });
});
