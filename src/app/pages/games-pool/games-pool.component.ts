import { Component, OnDestroy, OnInit } from '@angular/core';

import { Apollo } from 'apollo-angular';
import { concatMap, tap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';

import { AuthService } from '@shared/services/auth.service';
import { CommonService } from '@shared/services/common.service';
import { GAME_QUERIES } from '@graphql/operations';
import { Router } from '@angular/router';
import { UserModel } from '@shared/models/user.model';
import { FetchGame, FetchGamesQueryModel } from '@shared/models/operations.model';

@Component({
  selector: 'app-games-pool',
  templateUrl: './games-pool.component.html',
  styleUrls: ['./games-pool.component.scss']
})
export class GamesPoolComponent implements OnInit, OnDestroy {
  displayedColumnsPoolGames: string[] = ['index', 'createdAt', 'player1.name', 'play'];
  displayedColumnsCurrentGames: string[] = ['index', 'createdAt', 'turnsPlayed', 'currentTurn.name', 'actions'];
  poolGames: Array<FetchGame>;
  currentGames: Array<FetchGame>;
  currentUser: UserModel;
  loading = true;
  gameSubscription: Subscription;

  constructor(public apollo: Apollo,
              public auth: AuthService,
              public commonService: CommonService,
              public router: Router) {
    this.currentUser = this.auth.getCurrentUser();
  }

  ngOnInit(): void {
    const { id: USERID } = this.currentUser;
    let fetchGames: Array<FetchGame>;

    this.gameSubscription = this.apollo
      .watchQuery<FetchGamesQueryModel>({
        query: GAME_QUERIES.fetchGamesQuery,
        variables: {
          id: USERID
        },
        pollInterval: 3000
      })
      .valueChanges
      .pipe(
        concatMap((value, index) => index === 0 ?
          of(value).pipe(
            tap(() =>  {
              this.loading = false;
            })
          ) : of(value)
        )
      )
      .subscribe((result) => {
        fetchGames = result.data.fetchGames;
        this.currentGames = fetchGames.filter((e: FetchGame) => e.player1.id === USERID || e.player2.id === USERID);
        this.poolGames = fetchGames.filter((e: FetchGame) => {
          return (e.player1.id !== USERID && e.player2.id === '-1');
        });
      });
  }

  ngOnDestroy(): void {
    this.gameSubscription.unsubscribe();
  }

  formatDate = (date: string) => this.commonService.formatDate(date);

  play(gameId: string): void {
    this.commonService.log('Go to game');
    this.router.navigate(['/game/' + gameId]);
  }
}
