import { AbstractControl, ReactiveFormsModule, ValidationErrors } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { Apollo } from 'apollo-angular';

import { AuthService } from '@shared/services/auth.service';
import { apolloServiceStub, fetchPlayerLoginResponse } from '@shared/mock/operations.mock';
import { GAME_QUERIES } from '@graphql/operations';
import { LoginComponent } from './login.component';
import { loginFormMockValues } from '@shared/mock/app.mock';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const authServiceStub = {
    authenticate: jasmine.createSpy('authenticate')
  };
  const routerServiceStub = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule ],
      declarations: [ LoginComponent ],
      providers: [
        { provide: Apollo, useValue: apolloServiceStub },
        { provide: AuthService, useValue: authServiceStub },
        { provide: Router, useValue: routerServiceStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should render input elements', () => {
    const compiled = fixture.debugElement.nativeElement;
    const userInput = compiled.querySelector('input[id="user"]');
    const passwordInput = compiled.querySelector('input[id="password"]');

    expect(userInput).toBeTruthy();
    expect(passwordInput).toBeTruthy();
  });

  it('should have form validity', () => {
    const form = component.loginForm;
    const { userName, password } = form.controls;

    expect(form.valid).toBeFalsy();

    userName.setValue(loginFormMockValues.name);
    password.setValue(loginFormMockValues.password);

    expect(form.valid).toBeTruthy();
  });

  it('should have input errors', () => {
    const { userName } = component.loginForm.controls as { [key: string]: AbstractControl };

    expect((userName.errors as ValidationErrors).required).toBeTruthy();

    userName.setValue(loginFormMockValues.name);

    expect(userName.errors).toBeNull();
  });

  describe('Login Feature', () => {
    beforeEach(() => {
      const { userName, password } = component.loginForm.controls;

      userName.setValue(loginFormMockValues.name);
      password.setValue(loginFormMockValues.password);
    });

    it('should authenticate on submit form call', () => {
      const element = fixture.debugElement.query(By.css('#loginForm'));

      spyOn(component, 'login');
      element.triggerEventHandler('ngSubmit', null);
      fixture.detectChanges();

      expect(component.login).toHaveBeenCalled();
    });

    it('should login',  async () => {
      await component.login();
      fixture.detectChanges();

      expect(component.apollo.query).toHaveBeenCalledWith({
        query: GAME_QUERIES.fetchPlayerLoginQuery,
        variables: loginFormMockValues
      });
      expect(authServiceStub.authenticate).toHaveBeenCalledWith(fetchPlayerLoginResponse);
      expect(routerServiceStub.navigate).toHaveBeenCalledWith(['/home']);
    });
  });
});
