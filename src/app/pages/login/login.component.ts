import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Apollo} from 'apollo-angular';
import { ApolloQueryResult } from '@apollo/client/core';

import { AuthService } from '@shared/services/auth.service';
import { FetchPlayerLoginQueryModel } from '@shared/models/operations.model';
import { GAME_QUERIES } from '@graphql/operations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  logging = false;

  constructor(public auth: AuthService,
              public fb: FormBuilder,
              public apollo: Apollo,
              public router: Router) {
    this.loginForm = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  async login(): Promise<void> {
    const { userName, password } = this.loginForm.value;
    let response: ApolloQueryResult<FetchPlayerLoginQueryModel>;
    let fetchPlayerLogin;

    this.logging = true;
    response = await this.apollo.query<FetchPlayerLoginQueryModel>({
      query: GAME_QUERIES.fetchPlayerLoginQuery,
      variables: { name: userName, password }
    }).toPromise();
    fetchPlayerLogin = response.data.fetchPlayerLogin;
    this.logging = false;

    if (fetchPlayerLogin) {
      this.auth.authenticate(fetchPlayerLogin);
      this.router.navigate(['/home']);
    }
  }
}
