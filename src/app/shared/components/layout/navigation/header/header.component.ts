import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { UserModel } from '@shared/models/user.model';
import {AuthService} from '@shared/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: UserModel;
  @Output() public sidenavToggle = new EventEmitter();
  @Output() logoutClicked = new EventEmitter();

  constructor(public auth: AuthService) {
    this.currentUser = this.auth.getCurrentUser();
  }

  ngOnInit(): void {
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  public onLogoutClicked = () => {
    this.logoutClicked.emit();
  }
}
