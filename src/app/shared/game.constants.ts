const GAME_CONSTANTS = {
  initial_game_matrix: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ],
  cell_status: {
    HITTED: -2,
    MISS: -1,
    COVERED_NO_SHIP: 0,
  },
  cell_class_names: {
    covered: 'covered',
    hitted: 'hitted',
    miss: 'miss',
    not_allowed: 'notAllowed'
  },
  max_positions: {
    X: 9,
    Y: 9,
  },
  game_status: {
    waiting_opponent: 'Waiting for opponent',
    your_turn: 'Your turn',
    opponent_turn: 'Your opponent turn'
  },
  messages: {
    confirm_surrender: 'Are you sure you want to surrender?'
  }
};

export default GAME_CONSTANTS;
