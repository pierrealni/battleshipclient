import { GAME_MUTATIONS, GAME_QUERIES } from '@graphql/operations';
import GAME_CONSTANTS from '@shared/game.constants';

import { of } from 'rxjs';

export const mockUser = { id: 'test', name: 'test' };
export const mockGame = { id: 'test' };
export const mockMove = { x: 0, y: 0, newStatus: -1 };
export const mockMatrix = GAME_CONSTANTS.initial_game_matrix;
export const fetchPlayerLoginResponse = mockUser;
export const joinGameResponse = {
  actionAdded: { opponentName : 'test', actionDesc: 'test action' },
  matrix: [],
  response: 'test'
};
export const fetchGameResponse = {
  id: 'test',
  createdAt: 'test',
  player1: mockUser,
  player2: { id: 'test_', name: 'test_' },
  turnsPlayed: 0,
  status: ''
};
export const fetchGamesResponse = [
  fetchGameResponse,
  {
    id: 'test2',
    createdAt: 'test2',
    player1: { id: 'test2', name: 'test2' },
    player2: { id: '-1' },
    turnsPlayed: 0,
    status: ''
  }
];
export const makeMoveResponse = 'user clicked';
export const surrenderGameResponse = 'user surrendered';

export const apolloServiceStub = {
  query: jasmine.createSpy().and.callFake( ({ query, variables }) => {
      switch (query) {
        case GAME_QUERIES.fetchPlayerLoginQuery:
          return of( {
            data: { fetchPlayerLogin: fetchPlayerLoginResponse }
          });
        case GAME_QUERIES.fetchGameQuery:
          return of( {
            data: { fetchGame: fetchGameResponse }
          });
        default:
          return of();
      }
    }
  ),
  mutate: jasmine.createSpy().and.callFake( ({ mutation, variables }) => {
      switch (mutation) {
        case GAME_MUTATIONS.joinGame:
          return of( { data: { joinGame: joinGameResponse } });
        case GAME_MUTATIONS.makeMove:
          return of( { data: { makeMove: makeMoveResponse } });
        case GAME_MUTATIONS.surrenderGame:
          return of( { data: { surrenderGame: surrenderGameResponse } });
        default:
          return of();
      }
    }
  ),
  watchQuery: jasmine.createSpy().and.callFake( ({ query, variables, interval }) => {
      switch (query) {
        case GAME_QUERIES.fetchGamesQuery:
          return { valueChanges:
              of( {
              data: { fetchGames: fetchGamesResponse }
            })
          };
        case GAME_QUERIES.fetchMyFinishedGamesQuery:
          return { valueChanges:
              of( {
                data: { fetchMyFinishedGames: fetchGamesResponse }
              })
          };
        default:
          return of();
      }
    }
  ),
  create: () => {}
};
