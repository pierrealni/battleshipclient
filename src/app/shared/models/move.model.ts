export interface MoveModel {
  x: number;
  y: number;
  newStatus: number;
}
