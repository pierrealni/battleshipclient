export interface Turn {
  id: string;
  name: string;
}

export interface Player {
  id: string;
  name: string;
}

export interface Settings {
  matrix1: Array<Array<number>>;
  matrix2: Array<Array<number>>;
}

export interface FetchGame {
  id: string;
  createdAt: string;
  player1: Player;
  player2: Player;
  turnsPlayed: number;
  currentTurn: Turn;
  status: string;
  settings: Settings;
}

export interface FetchGameQueryModel {
  fetchGame: FetchGame;
}

///

export interface ActionAdded {
  actionDesc: string;
  actorId: string;
  key: number;
  opponentName?: string;
  gameIsSurrendered?: string;
  gameHasWinner?: boolean;
  nextTurnId?: string;
}

export interface JoinGame {
  actionAdded: ActionAdded;
  matrix: Array<Array<number>>;
  response: string;
}

export interface JoinGameMutationModel {
  joinGame: JoinGame;
}

///

export interface StartGame {
  matrix: Array<Array<number>>;
  response: string;
}

export interface StartGameMutationModel {
  startGame: StartGame;
}

//

export interface SurrenderGameMutationModel {
  surrenderGame: string;
}

//

export interface MakeMoveMutationModel {
  makeMove: string;
}

//

export interface ActionAddedSubscriptionModel {
  actionAdded: ActionAdded;
}

//

export interface FetchGamesQueryModel {
  fetchGames: Array<FetchGame>;
}

//

export interface FetchMyFinishedGamesQueryModel {
  fetchMyFinishedGames: Array<FetchGame>;
}

//

export interface FetchPlayerLoginQueryModel {
  fetchPlayerLogin: Player;
}
