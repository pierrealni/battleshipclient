import { Injectable } from '@angular/core';

import APP_CONSTANTS from '@shared/app.constants';
import { UserModel } from '@shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constants = APP_CONSTANTS;

  authenticate = (user: UserModel) => {
    localStorage.setItem(this.constants.auth_local_storage_key, JSON.stringify(user));
  }

  getCurrentUser = () => {
    let item: string;

    try {
      item = localStorage.getItem(this.constants.auth_local_storage_key) as string;
      if (!item) {
        return false;
      }

      return JSON.parse(item);
    } catch (e) {
      return false;
    }
  }

  isLoggedIn(): boolean {
    return !!this.getCurrentUser();
  }

  logOut = () => {
    localStorage.removeItem(this.constants.auth_local_storage_key);
  }
}
