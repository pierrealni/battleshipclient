import { Injectable } from '@angular/core';

import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  timeFormat = 'DD/MM/YYYY - hh:mm a';

  constructor() { }

  formatDate(value: string): string {
    return moment(value).format(this.timeFormat);
  }

  log(message: string, messageObject?: object | string): void {
    const log = `Application log: *** ${message}`;

    messageObject ? console.log(log, messageObject) : console.log(log);
  }
}

